<?php
    session_start();
    if(isset($_SESSION["autenticacion"]) && $_SESSION["autenticacion"] == "correcta")
    {
        include ('..\bd\conexion_db.php');
        $usuario = $_SESSION["usuario"];
        $sql = "select tu.nombre_usu,tu.usu_univ from dbo.c_usuario tu where tu.usu_univ = '".$usuario."'";
        
        $stmt = sqlsrv_query( $conn, $sql );
        if( $stmt === false) 
        {
            die( print_r( sqlsrv_errors(), true) );
        }
        $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
        if (is_null($row))
        {
            header("location:..\index.php?autenticacion=sin_permiso");
        }
        else
        {
            return($row); 
        }
        sqlsrv_free_stmt( $stmt);
    }
    else
    {
        header("location:..\index.php");
    }  

?>