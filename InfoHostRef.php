<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Inventario VMS</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sidebars/">
        <!-- Bootstrap core CSS -->
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="../jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
        <script src="../bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
        <script src="../bootstrap/js/sidebars.js" type="text/javascript"></script>
<!--        <script>
            $(document).ready(function(){
                var id_consulta = 5;
                    $.ajax({
                            data:  {id_consulta : id_consulta},
                            url:   '../bd/consultasdb.php',
                            type:  'GET',
                            beforeSend: function () 
                            {
                              //alert(data);
                            },
                            success:  function (response) 
                            {                 
                              //alert(response);    
                              $( "tbody#inventario" ).html(response);
                            },
                            error:function(response)
                            {
                              $( "tbody#inventario" ).html(response);
                            }
                    });
            });
        </script>    -->


        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
          }

          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
          .input-group-text {background: #18277e; color: #ffffff;}
/*        #global 
          {
            height: 470px;
            width: auto;
            overflow-y: scroll;
          }
*/
          
         
                   
        </style>
        <!-- Custom styles for this template -->
        <link href="../bootstrap/css/sidebars.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
       >  
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="home" viewBox="0 0 16 16">
                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
            </symbol>
            <symbol id="gear-fill" viewBox="0 0 16 16">
                <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
            </symbol>
            <symbol id="people-circle" viewBox="0 0 16 16">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </symbol>
        </svg>

        <main>
            <div class="container-fluid d-flex flex-column flex-shrink-0 p-3 bg-light fixed-left" style="width: 222px;">
                <div class="py-1 text-center">
                    <h5>TD Infraestructura</h5>
                </div>
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                    <img src="../img/logotelcel.png" alt="" width="180" height="50"/>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="principal.php" class="nav-link link-dark" aria-current="page">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Inicio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="inventario.php" class="nav-link active" aria-current="page">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Inventario
                        </a>
                    </li>
                    <li>
                        <a href="proyectos.php" class="nav-link link-dark" title="Consultar Proyectos." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Proyectos
                        </a>
                    </li>
                    <?php
                    if($id_perfil <> 3)
                    {    
                    ?>
                    <li>
                        <a href="captura_proyecto.php" class="nav-link link-dark" title="Dar de alta proyecto." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Registrar Proyecto
                        </a>
                    </li>
                    <li>
                        <a href="proyectosE.php" class="nav-link link-dark" title="Permite actualizar información de un proyecto." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Editar Proyecto
                        </a>
                    </li>
                    <li>
                        <a href="asociar_vm.php" class="nav-link link-dark" title="Permite asociar una vm a un proyecto." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Asociar VM
                        </a>
                    </li>
                    <?php
                    }
                    ?>
                    <li>
                        <a href="editar_vm.php" class="nav-link link-dark" title="Permite actualizar información de una VM." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Editar VM
                        </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link link-dark">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Reportes
                        </a>
                    </li>
                </ul>
                <hr>
                <div class="dropdown">
                    <a href="#" class="" id="dropdownUser2" data-bs-toggle="dropdown">
                      <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>

                      <span class="text-justify"><?php echo $nombre_usu; ?></span>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                      <li><a class="dropdown-item" href="salir.php">Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
            <div class="b-example-divider"></div>
            <section class="py-1 text-center container">
                <div class="row py-lg-3">
                        <div class="col-lg-11 mx-auto">
                        <h1 class="fw-light">Inventario VMs</h1>
                        <a class="btn btn-primary" href="descargar_inventario.php" role="button">Descargar CSV</a>
                        <br>
                        <div id="global">
<!--                            <table class="table table-striped" id="table_inv">
                                <thead>
                                    <tr>
                                        <th>ID PROYECTO</th>
                                        <th>PROYECTO</th>
                                        <th>NUM EMPLEADO</th>
                                        <th>SOLICITANTE</th>
                                        <th>DEPTO</th>
                                        <th>GERENCIA</th>
                                        <th>SUBDIRECCION</th>
                                        <th>DIRECCION</th>
                                        <th>FECHA RECEPCION PROYECTO</th>
                                        <th>CLUSTER</th>
                                        <th>ID VM</th>
                                        <th>NOMBRE VM (VCENTER)</th>
                                        <th>HOSTNAME</th>
                                        <th>AMBIENTE SOLICITADO</th>
                                        <th>CPU SOLICITADO</th>
                                        <th>CPU ENTREGADO</th>
                                        <th>RAM SOLICITADA (GB)</th>
                                        <th>RAM ENTREGADA (GB)</th>
                                        <th>STORAGE SOLICITADO (GB)</th>
                                        <th>STORAGE ENTREGADO (GB)</th>
                                        <th>SO SOLICITADO</th>
                                        <th>SO ENTREGADO</th>
                                        <th>BD SOLICITADA</th>
                                        <th>BD ENTREGADA</th>
                                        <th>ESTATUS VM</th>
                                        <th>FOLIO REMEDY</th>
                                        <th>FECHA SOLICITUD MOP</th>
                                        <th>FECHA RECEPCION VM</th>
                                        <th>IP</th>
                                        <th>FECHA ENTREGA USUARIO</th>
                                        <th>F60 ESTATUS</th>
                                        <th>PRE ATP ESTATUS</th>
                                        <th>FECHA INICIO PRE ATP</th>
                                        <th>FECHA FIN PRE ATP</th>
                                        <th>ATP ESTATUS</th>
                                        <th>FECHA INICIO ATP</th>
                                        <th>FECHA RECEPCION OYM RTO</th>
                                        <th>LICENCIAMIENTO WINDOWS</th>
                                        <th>LICENCIAMIENTO LINUX</th>
                                        <th>LICENCIAMIENTO BASE DATOS</th>
                                        <th>LICENCIAMIENTO ESTATUS</th>
                                        <th>MIGRACION</th>
                                        <th>CPU PROMEDIO MENSUAL</th>
                                        <th>MEMORIA PROMEDIO MENSUAL</th>
                                        <th>STORAGE PROMEDIO MENSUAL</th>
                                        <th>COMENTARIOS</th>
                                    </tr>
                                </thead>
                                <tbody id="inventario" class="text-uppercase">
                                </tbody>
                            </table>-->
                            <iframe width="1000" height="700" src="https://app.powerbi.com/view?r=eyJrIjoiODAxZDRjNDUtNjdiNi00ZTBkLWE0NGMtYWY3ZDU4N2Y5NjcwIiwidCI6IjM5MWY3M2I3LWE1OTAtNDNhZC05OGYwLThiMDUwOThhY2NjOCIsImMiOjR9&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>
                            
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </body>
</html>
