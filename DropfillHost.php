<?php
    include ('..\bd\conexion_db.php');

   //*************************************************************************** 
    //Programacion para NAGIOS OYM 
    $sqlPro = "EXEC [dbo].[CleanHost_Service];"; //Produccion
    //$sqlPro = "EXEC [dbo].[CleanHost_Service_Desarrollo];"; //Desarrollo
    $stmtPro = sqlsrv_query( $conn, $sqlPro );
    sqlsrv_free_stmt( $stmtPro);
    $url = "http://10.119.131.31/nagiosxi/api/v1/objects/host?apikey=POu7Mi9XZR05VESSsOqTW3c35hneY9T4eIqZZDegKekWScOEhuNdAYfUvV8OlhRC&pretty=1";
    $url = str_replace ( ' ', '%20', $url);
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_URL,$url);
    $result=curl_exec($handle);
    curl_close($handle);
    $result = file_get_contents($url);
    $array = json_decode($result, true);
    $IPNagios = "10.119.131.31";
    foreach ($array['host'] as $val) {
      $host =  $val['host_name'];
      $IPaddress =  $val['address'];    
      $sql_uno="INSERT INTO dbo.c_Host (host_name, address, srv_nagios, date_qry) VALUES ('$host', '$IPaddress ', '$IPNagios', getdate())"; //Produccion
      //$sql_uno="INSERT INTO dbo.c_Host_Desarrollo (host_name, address, srv_nagios, date_qry) VALUES ('$host', '$IPaddress ', '$IPNagios', getdate())"; //Desarrollo
      $stmt_uno = sqlsrv_query( $conn, $sql_uno );
        }
    $urlSr = "http://10.119.131.31/nagiosxi/api/v1/objects/service?apikey=POu7Mi9XZR05VESSsOqTW3c35hneY9T4eIqZZDegKekWScOEhuNdAYfUvV8OlhRC&pretty=1";
    $urlSr = str_replace ( ' ', '%20', $urlSr);
    $handleSr = curl_init();
    curl_setopt($handleSr, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handleSr, CURLOPT_URL,$urlSr);
    $resultSr=curl_exec($handleSr);
    curl_close($handleSr);
    $resultSr = file_get_contents($urlSr);
    $arraySr = json_decode($resultSr, true);  
    foreach ($arraySr['service'] as $val) {
      $hostS =  $val['host_name'];
      $SrvDes =  $val['service_description'];    
      $sqlChd = "select id_host from dbo.c_Host where host_name = '$hostS';"; //Produccion
      //$sqlChd = "select id_host from dbo.c_Host_Desarrollo where host_name = '$hostS';"; //Desarrollo
      $stmtChd = sqlsrv_query( $conn, $sqlChd );
      $row = sqlsrv_fetch_array( $stmtChd, SQLSRV_FETCH_ASSOC);
      if (isset($row['id_host'])) 
	      {
          $id_host = $row['id_host'];
          if (is_null($id_host))
      {}
      else
      {
      sqlsrv_free_stmt($stmtChd);
      $sql_dos="INSERT INTO dbo.c_Service (host_name, service_description, srv_nagios, date_qry, id_host) VALUES ('$hostS', '$SrvDes ', '$IPNagios', getdate(), $id_host)"; //Produccion
      //$sql_dos="INSERT INTO dbo.c_Service_Desarrollo (host_name, service_description, srv_nagios, date_qry, id_host) VALUES ('$hostS', '$SrvDes ', '$IPNagios', getdate(), $id_host)"; //Desarrollo
      $stmt_dos = sqlsrv_query( $conn, $sql_dos );
      }
	      }      
    }
    //*************************************************************************** 
    //Programacion para NAGIOS Informatica
    $urlI = "http://10.191.143.214/nagiosxi/api/v1/objects/host?apikey=mLBmY5cGbjUrSN5VoRugCNhg4PTWLYqnZ03XlfTQRNhKQFVfMpOltlUQbNNcLdSK&pretty=1";
    $urlI = str_replace ( ' ', '%20', $urlI);
    $handleI = curl_init();
    curl_setopt($handleI, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handleI, CURLOPT_URL,$urlI);
    $resultI=curl_exec($handleI);
    curl_close($handleI);
    $resultI = file_get_contents($urlI);
    $arrayI = json_decode($resultI, true);
    $IPNagiosI = "10.191.143.214";
    foreach ($arrayI['host'] as $valI) {
      $hostI =  $valI['host_name'];
      $IPaddressI =  $valI['address'];    
      $sql_unoI="INSERT INTO dbo.c_Host (host_name, address, srv_nagios, date_qry) VALUES ('$hostI', '$IPaddressI ', '$IPNagiosI', getdate())"; //Produccion
      //$sql_unoI="INSERT INTO dbo.c_Host_Desarrollo (host_name, address, srv_nagios, date_qry) VALUES ('$hostI', '$IPaddressI ', '$IPNagiosI', getdate())"; //Desarrollo
      $stmt_unoI = sqlsrv_query( $conn, $sql_unoI );
        }
    $urlSrI = "http://10.191.143.214/nagiosxi/api/v1/objects/service?apikey=mLBmY5cGbjUrSN5VoRugCNhg4PTWLYqnZ03XlfTQRNhKQFVfMpOltlUQbNNcLdSK&pretty=1";
    $urlSrI = str_replace ( ' ', '%20', $urlSrI);
    $handleSrI = curl_init();
    curl_setopt($handleSrI, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handleSrI, CURLOPT_URL,$urlSrI);
    $resultSrI=curl_exec($handleSrI);
    curl_close($handleSrI);
    $resultSrI = file_get_contents($urlSrI);
    $arraySrI = json_decode($resultSrI, true);  
    foreach ($arraySrI['service'] as $valIn) {
      $hostSI =  $valIn['host_name'];
      $SrvDesI =  $valIn['service_description'];    
      $sqlChdI = "select id_host from dbo.c_Host where host_name = '$hostSI';"; //Produccion
      //$sqlChdI = "select id_host from dbo.c_Host_Desarrollo where host_name = '$hostSI';"; //Desarrollo
      $stmtChdI = sqlsrv_query( $conn, $sqlChdI );
      $rowI = sqlsrv_fetch_array( $stmtChdI, SQLSRV_FETCH_ASSOC);
      if (isset($rowI['id_host'])) 
	      {
          $id_hostI = $rowI['id_host'];
          if (is_null($id_hostI))
      {}
      else
      {
      sqlsrv_free_stmt($stmtChdI);
      $sql_dosI="INSERT INTO dbo.c_Service (host_name, service_description, srv_nagios, date_qry, id_host) VALUES ('$hostSI', '$SrvDesI ', '$IPNagiosI', getdate(), $id_hostI)"; //Produccion
      //$sql_dosI="INSERT INTO dbo.c_Service_Desarrollo (host_name, service_description, srv_nagios, date_qry, id_host) VALUES ('$hostSI', '$SrvDesI ', '$IPNagiosI', getdate(), $id_hostI)"; //Desarrollo
      $stmt_dosI = sqlsrv_query( $conn, $sql_dosI );
      }
	      }      
    }
    echo "Se han actualizado los regitros de host y servicios para Nagios OYM e Informatica";
?>