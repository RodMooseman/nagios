<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!-- <link href="../bootstrap/css/bootstrap.min.222.css" rel="stylesheet" media="screen"> -->
    <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>

<body>    
<div class="input-group input-group-sm">
    <span class="input-group-text" id="inputGroup-sizing-sm">Fecha Final</span>
    <div class="controls input-append date form_datetime" data-date="" data-date-format="dd-mm-yyyy hh:ii" data-link-field="dtp_input2">
    <input size="30" type="text" value="" id="valor2" >                    
    <span class="add-on"><i class="icon-remove"></i></span>
	<span class="add-on"><i class="icon-th"></i></span>
    </div>
	<input type="hidden" id="dtp_input2" value=""/><br/>
</div>
<br>
<script type="text/javascript" src="../jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../js/locales/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });	
</script>
</body>
</html>
