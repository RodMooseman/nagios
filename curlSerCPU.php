<?php
    include ('..\bd\conexion_db.php');
    //sqlsrv_begin_transaction( $conn );
    $DateI = $_POST['valorCaja1'];
    //$contador=1;
    //$contador2=1;
    $time_inicial = strtotime($DateI); 
    $DateF = $_POST['valorCaja2'];
    $time_final = strtotime($DateF);
    $fp = fopen("../logs/LogPro.log", "a");
    $thisTime = new DateTime();
    $thisTime = $thisTime->format('Y-m-d-H-i-s');
    fwrite($fp, "Se inicia el proceso de insercion con fecha " . $thisTime . "----------------------------------------\n");
    fclose($fp);
    //------------------------------------------------
    //--- Nagios OYM -----------------
    $sqlSome = "select TOP 1 id_host from dbo.c_Host ORDER BY id_host DESC;";
    $stmtSome= sqlsrv_query( $conn, $sqlSome );
    $some=sqlsrv_fetch_array($stmtSome, SQLSRV_FETCH_ASSOC);
    $sqlChd = "select id_host,host_name, srv_nagios, (select count(1) from dbo.c_Host) as totalHost from dbo.c_Host;"; // Produccion
    //$sqlChd = "select host_name, srv_nagios, (select count(1) from dbo.c_Host) as totalHost from dbo.c_Host;"; // Desarrollo
    $stmtChd = sqlsrv_query( $conn, $sqlChd );
    if( $stmtChd === false) 
    {
        $fp = fopen("../logs/LogPro.log", "a");
        $thisTime = new DateTime();
        $thisTime = $thisTime->format('Y-m-d-H-i-s');
        fwrite($fp, "\nERROR!!\nSe termina  proceso de insercion debido a error en consulta a DB Host , fecha " . $thisTime . " +++++++++++++++++++++++++++++++++++\n");
        fclose($fp);
        die( print_r( sqlsrv_errors(), true) );
    }
    else
    {
        while( $rowt = sqlsrv_fetch_array($stmtChd, SQLSRV_FETCH_ASSOC)) 
        {       
            $idHostActual = $rowt['id_host'];
            $hostname = $rowt['host_name'];
            $srv_nags = $rowt['srv_nagios'];
            $tlTotalR = $rowt['totalHost'];
            $fp = fopen("../logs/RegistroActual.log", "w+");
            $thisTime = new DateTime();
            $thisTime = $thisTime->format('Y-m-d-H-i-s');
            fwrite($fp, "Actualmente en el registro  " . $idHostActual . " de ". $some['id_host'] . " " . $hostname . "\n\nUltima actualizacion " . $thisTime );
            fclose($fp);
            $sqlShd = "select service_description from dbo.c_Service where (host_name = '$hostname') and (service_description like '%DE CPU%');"; //Produccion
            //$sqlShd = "select service_description from dbo.c_Service_Desarrollo where (host_name = '$hostname') and (service_description like '%DE CPU%');"; //Desarrollo
            $stmtShd = sqlsrv_query( $conn, $sqlShd );
            if( $stmtShd === false) 
            {
                $fp = fopen("../logs/LogPro.log", "a");
                $thisTime = new DateTime();
                $thisTime = $thisTime->format('Y-m-d-H-i-s');
                fwrite($fp, "\nERROR!!\nSe termina  proceso de insercion debido a error en consulta a DB Service , fecha " . $thisTime . " +++++++++++++++++++++++++++++++++++\n");
                fclose($fp);
                die( print_r( sqlsrv_errors(), true) );
            }
            else
            {
               while( $rowS = sqlsrv_fetch_array($stmtShd, SQLSRV_FETCH_ASSOC)) 
               {
                    //if($contador2<100)
                    //{
                    //$contador2=$contador2+1;     
                    $service = $rowS['service_description'];    
                    if($srv_nags == "10.119.131.31")
                    {
                        $Nag = "10.119.131.31";                        
                        $url = "http://$Nag//nagiosxi/api/v1/objects/rrdexport?apikey=LcJ7vcsakoP5gc7NrCBAYvWXbdDC8Cl95kfTAL3Tp9fIkls5WjEc9Q9j32bbUvDY&pretty=1&host_name=$hostname&service_description=$service&start=$time_inicial&end=$time_final&step=900";
                    }
                    elseif($srv_nags == "10.191.143.214")
                    {
                        $Nag = "10.191.143.214";                        
                        $url = "http://$Nag//nagiosxi/api/v1/objects/rrdexport?apikey=mLBmY5cGbjUrSN5VoRugCNhg4PTWLYqnZ03XlfTQRNhKQFVfMpOltlUQbNNcLdSK&pretty=1&host_name=$hostname&service_description=$service&start=$time_inicial&end=$time_final&step=900";
                    } 
                    $url = str_replace ( ' ', '%20', $url);
                    //fwrite($fp, "\nUrl:\n".$url);    
                    $handle = curl_init();
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($handle, CURLOPT_URL,$url);
                    $result=curl_exec($handle);
                    curl_close($handle);
                    $result = file_get_contents($url);
                    //fwrite($fp, "\nStrJson:".$result);
                    $jsonData = json_decode($result, true);
                    if (isset($jsonData['data']['row'])) 
                    {
                        //fwrite($fp, "\nF1"); 
                        foreach($jsonData["data"]['row'] as $val1)
                        {
                            //fwrite($fp, "\nF2"); 
                            if(isset($val1['v']))
                            {
                                //fwrite($fp, "\nF3"); 
                                if((is_numeric($val1['v']) && $val1['t']!="")||is_array($val1['v']))
                                {
                                    //fwrite($fp, "\nF4"); 
                                    $tempValV="";
                                    if(is_array($val1['v']))
                                    {
                                        //fwrite($fp,print_r($val1['v'], TRUE)); 
                                        if(is_numeric($val1['v'][1]))     
                                        { 
                                            $tempValV = floatval($val1['v'][1]);
                                            $tempValV  =  rtrim(number_format($tempValV ,2),0);
                                        }
                                        else
                                            $tempValV="";
                                    }
                                    else
                                    {
                                        $tempValV  = floatval($val1['v']);
                                        $tempValV  =  rtrim(number_format($tempValV ,2),0);
                                    }
                                    //fwrite($fp, "\n".$contador2."   FlagTV ".$tempValV);
                                    if($tempValV != "")
                                    {
                                        $fecha_t =  $val1['t'];              
                                        $fecha_t = date('Y-m-d H:i:s', $fecha_t);
                                        $service = trim($service);
                                        $so = substr($service, strpos($service, "SO") + 3, strpos($service, ":") - strlen($service));
                                        $sqlRev = "SELECT id_CPU_S FROM dbo.c_MetCPU WHERE (host_name = '$hostname') and (date_t = CONVERT (datetime, '$fecha_t', 121));"; //Produccion
                                        //$sqlRev = "SELECT id_CPU_S FROM dbo.c_MetCPU_Desarrollo WHERE (host_name = '$hostname') and (date_t = CONVERT (datetime, '$fecha_t', 121));"; //Desarrollo
                                        $stmtRev = sqlsrv_query( $conn, $sqlRev );
                                        $Id_Reg = 0;
                                        if( $stmtRev === false) 
                                        {
                                            $fp = fopen("../logs/LogPro.log", "a");
                                            $thisTime = new DateTime();
                                            $thisTime = $thisTime->format('Y-m-d-H-i-s');
                                            fwrite($fp, "\nERROR!!\nSe termina  proceso de insercion debido a error en consulta a DB Merticas CPU , fecha " . $thisTime . " +++++++++++++++++++++++++++++++++++\n");
                                            fclose($fp);
                                            die( print_r( sqlsrv_errors(), true) );
                                        }
                                        else
                                        {
                                            while( $rowR = sqlsrv_fetch_array($stmtRev, SQLSRV_FETCH_ASSOC))
                                                $Id_Reg = $rowR['id_CPU_S'];
                                            if(is_null($Id_Reg))
                                                $Id_Reg = 0;
                                            if($Id_Reg == 0)
                                            {
                                                $sql_ins="INSERT INTO dbo.c_MetCPU (host_name, so, valor_cpu, date_t, address_nag, date_ins) VALUES ('$hostname', '$so', '$tempValV ', '$fecha_t' , '$Nag' ,getdate())"; //Produccion
                                                //$sql_ins="INSERT INTO dbo.c_MetCPU_Desarrollo (host_name, so, valor_cpu, date_t, address_nag, date_ins) VALUES ('$hostname', '$so', '$valorCPU', '$fecha_t' , '$Nag' ,getdate())"; //Desarrollo
                                                $stmt_ins = sqlsrv_query( $conn, $sql_ins );   
                                                //fwrite($fp, "\n".$contador."- ".$sql_ins);
                                                //$contador2=$contador2+1;
                                            }    
                                        }
                                        sqlsrv_free_stmt($stmtRev);
                                    }
                                    
                                }
                            }
                        }
                    }
                    //}
               }
            }
            sqlsrv_free_stmt($stmtShd);
        } 
        sqlsrv_free_stmt($stmtChd);
    }
    $fp = fopen("../logs/LogPro.log", "a");
    $thisTime = new DateTime();
    $thisTime = $thisTime->format('Y-m-d-H-i-s');
    fwrite($fp, "\nSe termina exitosamente proceso de insercion con fecha " . $thisTime . " +++++++++++++++++++++++++++++++++++\n");
    fclose($fp);
    echo "Registros cargados con exito.";
?>